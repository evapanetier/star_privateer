Analysis pipeline
#################

.. automodule:: star_privateer

.. autofunction:: star_privateer.analysis_pipeline


Lomb-Scargle periodogram
************************

.. autofunction:: star_privateer.compute_lomb_scargle

.. autofunction:: star_privateer.compute_prot_err_gaussian_fit_chi2_distribution

.. autofunction:: star_privateer.plot_ls


Auto-correlation function (ACF)
*******************************

.. autofunction:: star_privateer.compute_acf

.. autofunction:: star_privateer.find_period_acf

.. autofunction:: star_privateer.plot_acf


Composite spectrum (CS)
***********************

.. autofunction:: star_privateer.compute_cs

.. autofunction:: star_privateer.compute_prot_err_gaussian_fit

.. autofunction:: star_privateer.plot_cs

Photometric index (Sph)
***********************

.. autofunction:: star_privateer.compute_sph

.. autofunction:: star_privateer.compute_lomb_scargle_sph

Wavelet analysis
****************

.. autofunction:: star_privateer.compute_wps

.. autofunction:: star_privateer.plot_wps

