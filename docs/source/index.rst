
star-privateer: stellar rotation and activity
==============================================

Welcome to the documentation of **star-privateer**. The present module provides a complete API to implement tools for stellar surface rotation and activity analysis in photometric light curves collected by space missions such as NASA/*Kepler*, NASA/TESS or, in a near future, `ESA/PLATO <https://platomission.com/>`_. Several tutorials are included in order to help new users that would like to discover the code. 


The module is developed at the `Osservatorio astrofisico di Catania <https://www.oact.inaf.it/>`_ (INAF-OACT).

Contact address: sylvain.breton@inaf.it

Documentation
#############

.. toctree::
   :maxdepth: 1
   :caption: User guide

   usage/installation/installation
   usage/fourier_analysis/fourier_analysis
   usage/timeseries_analysis/timeseries_analysis
   usage/rooster_training_framework/rooster_training_framework
   usage/stellar_analysis_framework/stellar_analysis_framework
   usage/cs_rooster_sph_analysis/cs_rooster_sph_analysis
   usage/cycle_determination/cycle_determination
   usage/wavelet_analysis/wavelet_analysis


.. toctree::
   :maxdepth: 2
   :caption: Detailed API

   usage/api/analysis_pipeline
   usage/api/rooster
